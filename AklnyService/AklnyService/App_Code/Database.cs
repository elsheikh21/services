﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ECS_Framework;
/// <summary>
/// Summary description for Database
/// </summary>
public class Database
{
    public Database()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string getLDAPServer()//Gets the LDAP Server IP from the settings table.
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT LDAPServer FROM Settings";
        string LDAPServer = dal.Query().Rows[0]["LDAPServer"].ToString();
        return LDAPServer;
    }

    public bool setLDAPServer(string LDAPServer)//Sets the LDAP Server IP in the settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer("192.168.2.40", "username", "12345", "etisalat");
        dal.sqlStatment = "UPDATE Settings SET LDAPServer = '" + LDAPServer + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getLDAPUsername()//Gets the LDAP Username from the settings table.
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT LDAPUsername FROM Settings";
        string LDAPUsername = dal.Query().Rows[0]["LDAPUsername"].ToString();
        return LDAPUsername;
    }

    public bool setLDAPLDAPUsername(string LDAPUsername)//Sets the LDAP Username in the settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET LDAPUsername = '" + LDAPUsername + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getLDAPPassword()//Gets the LDAP Password from the settings table.
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT LDAPPassword FROM Settings";
        string LDAPPassword = dal.Query().Rows[0]["LDAPPassword"].ToString();
        return LDAPPassword;
    }

    public bool setLDAPPassword(string LDAPPassword)//Sets the LDAP Password in the settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET LDAPPassword = '" + LDAPPassword + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getDistinguishedName()//Get Distinguished Name from settings table.
    {

        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT DistinguishedName FROM Settings";
        string distinguishedName = dal.Query().Rows[0]["DistinguishedName"].ToString();
        return distinguishedName;
    }

    public bool setDistinguishedName(string distinguishedName)//Set Distinguished Name to settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET DistinguishedName = '" + distinguishedName + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getWebPath()//Get webittend text file path from settings table.
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT OTMSWebPath FROM settings";
        string OTMSWebPath = dal.Query().Rows[0]["OTMSWebPath"].ToString();
        return OTMSWebPath;

    }

    public bool setWebPath(string OTMSWebPath)//Set webittend text file path to settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET OTMSWebPath = '" + OTMSWebPath + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getMobilePath()//Get mobittend text file path from settings table.
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT OTMSMobilePath FROM settings";
        string OTMSMobilePath = dal.Query().Rows[0]["OTMSMobilePath"].ToString();
        return OTMSMobilePath;
    }

    public bool setMobilePath(string OTMSMobilePath)//Set mobittend text file path to settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET OTMSMobilePath = '" + OTMSMobilePath + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getLogPath()//Get log text file path from settings table.
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT LogPath FROM settings";
        string logPath = dal.Query().Rows[0]["LogPath"].ToString();
        return logPath;

    }

    public bool setLogPath(string logPath)//Set log text file path to settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET LogPath = '" + logPath + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public bool userExists(string username)//Check if user exists in users table.
    {
        bool flag = false;
        Database database = new Database();
        //MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        //dal.sqlStatment = "SELECT COUNT(username) AS COUNT FROM users WHERE username='" + username + "'";
        //int count = int.Parse(dal.Query().Rows[0]["COUNT"].ToString());
       
        DataTable users = database.getUsers();
        int count=users.Select("Username='" + username+"'").Count();
        if (count > 0)
            flag = true;

        return flag;
    }

    public bool userExistsByID(string userID)//Check if user exists in users table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        Database database = new Database();
        //dal.sqlStatment = "SELECT COUNT(userID) AS COUNT FROM users WHERE userID='" + userID + "'";
        //int count = int.Parse(dal.Query().Rows[0]["COUNT"].ToString());
        DataTable users = database.getUsers();
        int count = users.Select("UserID='" + userID + "'").Count();
        if (count > 0)
            flag = true;

        return flag;
    }

    public string getUsername(string userid)//Get user id from users table.
    {
        string username = "";
        if (userExistsByID(userid))
        {
            MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
            dal.sqlStatment = "SELECT username FROM users WHERE UserID='" + userid + "'";
            username = dal.Query().Rows[0]["username"].ToString();
        }
        return username;
    }

    public string getUserID(string username)//Get user id from users table.
    {
        string userID = "";
        if (userExists(username))
        {
            MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
            dal.sqlStatment = "SELECT UserID FROM users WHERE username='" + username + "'";
            userID = dal.Query().Rows[0]["UserID"].ToString();
        }
        return userID;
    }

    public string getFirstName(string username)//Get user's first name from users table.
    {
        string fName = "";
        if (userExists(username))
        {
            MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
            dal.sqlStatment = "SELECT fName FROM users WHERE username='" + username + "'";
            fName = dal.Query().Rows[0]["fName"].ToString();
        }
        return fName;
    }

    public string getLastName(string username)//Get user's last name from users table.
    {
        string lName = "";
        if (userExists(username))
        {
            MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
            dal.sqlStatment = "SELECT lName FROM users WHERE username='" + username + "'";
            lName = dal.Query().Rows[0]["lName"].ToString();
        }
        return lName;
    }

    public DataTable getSettings()//Get settings from settings table.
    {
        DataTable settings = null;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT * FROM Settings";
        settings = dal.Query();
        return settings;
    }

    //    public bool setSettings()
    //    {
    //        bool flag = false;
    //        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
    //        dal.sqlStatment = "UPDATE Settings SET Port = '" + port + "'";
    //        if (dal.NonQuery(IsolationLevel.Serializable))
    //            flag = true;
    //        return flag;
    //    }


    //OTMSMobilePath
    //OTMSWebPath
    //License
    //Port
    //SMTPServer	
    //SMTPUserName
    //SMTPPassword
    //SSL
    //FromName
    //Subject	
    //isBodyHtml

    public string getDomain()//Get domain from the settings table.
    {
        string domain = "";
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT Domain FROM Settings";
        domain = dal.Query().Rows[0]["Domain"].ToString();
        return domain;
    }

    public string getPort()//Get port from settings table.
    {
        string port = "";
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT Port FROM Settings";
        port = dal.Query().Rows[0]["Port"].ToString();
        return port;
    }

    public bool setPort(string port)//Set port to settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET Port = '" + port + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getSMTPServer()//Get SMTP Server from settings table.
    {
        string SMTPServer = "";
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT SMTPServer FROM Settings";
        SMTPServer = dal.Query().Rows[0]["SMTPServer"].ToString();
        return SMTPServer;
    }

    public bool setSMTPServer(string SMTPServer)//Set SMTP Server to settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET SMTPServer = '" + SMTPServer + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getSMTPUserName()//Get SMTP username from settings table.
    {
        string SMTPUserName = "";
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT SMTPUserName FROM Settings";
        SMTPUserName = dal.Query().Rows[0]["SMTPUserName"].ToString();
        return SMTPUserName;
    }

    public bool setSMTPUserName(string SMTPUserName)//Set SMTP UserName to settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET SMTPUserName = '" + SMTPUserName + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getSMTPPassword()//Get SMTP password from settings table.
    {
        string SMTPPassword = "";
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT SMTPPassword FROM Settings";
        SMTPPassword = dal.Query().Rows[0]["SMTPPassword"].ToString();
        return SMTPPassword;
    }

    public bool setSMTPPassword(string SMTPPassword)//Set SMTP password to settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET SMTPPassword = '" + SMTPPassword + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getSSL()//Get SSL from settings table.
    {
        string SSL = "";
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT SSL FROM Settings";
        SSL = dal.Query().Rows[0]["SSL"].ToString();
        return SSL;
    }

    public bool setSSL(string SSL)//Set SSL password to settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET SSL = '" + SSL + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getFromName()//Get from name from the settings table.
    {
        string fromName = "";
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT FromName FROM Settings";
        fromName = dal.Query().Rows[0]["FromName"].ToString();
        return fromName;
    }

    public bool setFromName(string fromName)//Set SSL password to settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET FromName = '" + fromName + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getSubject()//Get subject from the settings table.
    {
        string subject = "";
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT Subject FROM Settings";
        subject = dal.Query().Rows[0]["Subject"].ToString();
        return subject;
    }

    public bool setSubject(string subject)//Set subject to settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET Subject = '" + subject + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public string getisBodyHtml()//Get isBodyHtml from settings table.
    {
        string isBodyHtml = "";
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT isBodyHtml FROM Settings";
        isBodyHtml = dal.Query().Rows[0]["isBodyHtml"].ToString();
        return isBodyHtml;
    }

    public bool setisBodyHtml(string isBodyHtml)//Set isBodyHtml to settings table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Settings SET isBodyHtml = '" + isBodyHtml + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public bool isAdmin(string userID)//Check is the user an administrator
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT isAdmin FROM Users WHERE UserID='" + userID + "'";
        DataTable dt = dal.Query();
        int isAdmin = 0;
        if (dt.Rows.Count > 0)
        {
            isAdmin = int.Parse(dal.Query().Rows[0]["isAdmin"].ToString());
        }
        
        if (isAdmin > 0)
            flag = true;

        return flag;
    }

    public bool licenseExistsByUserID(string userID)//Check if license exists in license table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT COUNT(UserID) AS COUNT FROM license WHERE UserID='" + userID + "'";
        int count = int.Parse(dal.Query().Rows[0]["COUNT"].ToString());

        if (count > 0)
            flag = true;

        return flag;
    }

    public bool licenseExistsbyLicense(string license)//Check if license exists in license table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT COUNT(License) AS COUNT FROM license WHERE License='" + license + "'";
        int count = int.Parse(dal.Query().Rows[0]["COUNT"].ToString());

        if (count > 0)
            flag = true;

        return flag;
    }

    public string getLicense(string userID)//Get license for user from license table.
    {
        string license = "";
        if (licenseExistsByUserID(userID))
        {
            MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
            dal.sqlStatment = "SELECT License FROM license WHERE UserID='" + userID + "'";
            license = dal.Query().Rows[0]["License"].ToString();
        }
        return license;
    }

    public bool isLicenseUsed(string license)//Get license status for user from license table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT Used FROM license WHERE License='" + license + "'";
        int used = int.Parse(dal.Query().Rows[0]["Used"].ToString());

        if (used > 0)
            flag = true;

        return flag;
    }

    public bool setLicense(string userID, string license)//Set license for user in license table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "INSERT INTO License (UserID, License) VALUES ('" + userID + "', '" + license + "')";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public DataTable getATTAB()//Get attendance absence reasons
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT ATT_ABS_REASON,ATT_ABS_REASON_TEXT_EN FROM ATTAB";
        DataTable ATTAB = dal.Query();
        ATTAB.TableName = "ATTAB";
        return ATTAB;
    }

    public DataTable getEventTypes()
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT * FROM Events";
        DataTable events = dal.Query();
        events.TableName = "Events";
        return events;
    }

    public DataTable getFavouriteStores(string userID)//Get favourite stores
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT * FROM FavouriteStores WHERE UserID='" + userID + "'";
        DataTable favouriteStores = dal.Query();
        favouriteStores.TableName = "favouriteStores";
        return favouriteStores;
    }

    public DataTable getFavouriteStoresUpdates(string userID)
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT * FROM favouritestoresupdates WHERE UserID='" + userID + "'";
        DataTable favouriteStores = dal.Query();
        favouriteStores.TableName = "favouriteStores";
        return favouriteStores;
    }

    //public bool addUser(string id, string username, string fname, string lname, string email)//Add user to users table.
    //{
    //    bool flag = false;
    //    MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
    //    dal.sqlStatment = "INSERT INTO users(UserID, Username, fName, lName, email) VALUES ('" + id + "'," + "'" + username + "'," + "'" + fname + "'," + "'" + lname + "'," + "'" + email + "')";
    //    flag = dal.NonQuery(IsolationLevel.Serializable);
    //    return flag;
    //}

    public bool addUser(string id, string username, string password, string fname, string lname, string email)//Add user to users table.
    {
        bool flag = false;
        Utilities utilities = new Utilities();
        username = username.ToLower().Trim();
        password = password.Trim();

        ///////////TEST LOGGING
        if (id.Equals("10000035") || id.Equals("100"))
        {
            Database database = new Database();
            string path = database.getLogPath();
            string logfile = "MobittendLog" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            utilities.WriteToFile(path, logfile, id+" ADDUSER:Password=" + password);
        }
        ///////////

        password = utilities.encrypt_pass(username, password);

        ///////////TEST LOGGING
        if (id.Equals("10000035") || id.Equals("100") )
        {
            Database database = new Database();
            string path = database.getLogPath();
            string logfile = "MobittendLog" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            utilities.WriteToFile(path, logfile, id+" ADDUSER:Password Encrypted=" + password);
        }
        ///////////

        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "INSERT INTO users(UserID, Username,Password ,fName, lName, email,RegKey) VALUES ('" + id + "'," + "'" + username + "','" + password + "','" + fname + "','" + lname + "','" + email + "','"+utilities.encrypt_pass(username,username)+"')";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public bool isEmailExists(string email)
    {
        bool flag = false;
        Database database = new Database();

        //MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        //dal.sqlStatment = "SELECT COUNT(*) AS COUNT FROM Users WHERE email='" + email + "'";
        //int count = int.Parse(dal.Query().Rows[0]["COUNT"].ToString());
        DataTable users = database.getUsers();
        int count = users.Select("email='" + email + "'").Count();
        if (count > 0)
            flag = true;
        return flag;
    }

    public bool isDeviceExists(string IMEI)
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT COUNT(*) AS COUNT FROM Devices WHERE IMEI='" + IMEI + "'";
        int count = int.Parse(dal.Query().Rows[0]["COUNT"].ToString());

        if (count > 0)
            flag = true;

        return flag;
    }

    public bool isUserIDExists(string userID)
    {
        bool flag = false;
        Database database = new Database();
        //MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        //dal.sqlStatment = "SELECT COUNT(*) AS COUNT FROM Users WHERE UserID='" + userID + "'";
        //int count = int.Parse(dal.Query().Rows[0]["COUNT"].ToString());
        DataTable users = database.getUsers();
        int count = users.Select("UserID='" + userID + "'").Count();
        if (count > 0)
            flag = true;
        
        return flag;
    }

    public bool addDevice(string IMEI, string userID, string simSerialNumber, string networkOperatorName, string subscriberID, string OSVersion, string model, string mobittendVersion)//Add device in the devices table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "INSERT INTO devices(IMEI,UserID,SimSerialNumber,NetworkOperatorName,SubscriberID,OSVersion,Model,MobittendVersion) VALUES ('"
        + IMEI + "','" + userID + "','" + simSerialNumber + "','" + networkOperatorName + "','" + subscriberID + "','" + OSVersion + "','" + model + "','" + mobittendVersion + "')";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public bool deleteDevice(string IMEI,string userID)//Delete device from the devices table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "DELETE FROM devices WHERE IMEI='" + IMEI + "' AND UserID='"+userID+"'";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public bool deleteUser(string UserID)//Delete user from the users table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "DELETE FROM Users WHERE UserID='" + UserID + "' DELETE FROM Devices WHERE UserID='" + UserID + "' DELETE FROM License WHERE UserID='" + UserID + "'";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public bool updateLicense(string license, string type)//Update license for a certain user in the license table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE license SET used='" + type + "' WHERE License='" + license + "'";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public bool updateUserStoreFlag(string userID)//Update stores' update flag for a certain user in the users table.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = " UPDATE users SET Updated= '1' WHERE UserID='" + userID + "'";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public string getUserIDFromLicense(string license)//Get user ID form license from license table.
    {
        string userID = "";
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT UserID FROM license WHERE License='" + license + "'";
        userID = dal.Query().Rows[0]["UserID"].ToString();

        return userID;
    }

    public bool setNumberOfDevices(string userID, string numberOfDevices)//set number of devices to a certain user
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE users SET DevicesNo='" + numberOfDevices + "' WHERE UserID='" + userID + "'";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public int getNumberOfDevices(string userID)//Get number of devices that can be assigned to a user
    {
        int number = -1;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT DevicesNo FROM users WHERE UserID='" + userID + "'";
        number = int.Parse(dal.Query().Rows[0]["DevicesNo"].ToString());

        return number;
    }

    public bool mobileFavouritesUpdated(string userID)//Mark user for updating his favourite stores
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE users SET Updated ='0' WHERE UserID='" + userID + "'";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public bool addStore(string storeName, string storeAddress, string storeLatitude, string storeLongitude, string storeDistance, string storeAccuracy, string approved, string createdBy, string createdFrom)//Add store
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "INSERT INTO stores (StoreName, StoreAddress, StoreLatitude, StoreLongitude,StoreDistance,StoreAccuracy,StoreApproved,CreatedBy,CreatedFrom) " +
            "VALUES ('" + storeName + "', '" + storeAddress + "', '" + storeLatitude + "', '" + storeLongitude + "', '" + storeDistance + "','" + storeAccuracy + "','" + approved + "','" + createdBy + "','" + createdFrom + "')";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public bool isUserExist(string username)
    {
        bool flag = false;
        Database database = new Database();
        //MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        //dal.sqlStatment = "SELECT COUNT(*) AS Count FROM users";
        //int count = int.Parse(dal.Query().Rows[0]["Count"].ToString());
        DataTable users = database.getUsers();
        int count = users.Select("Username='" + username + "'").Count();
        if (count > 0)
            flag = true;
        return flag;
    }


    public DataTable UserInfo(string username,string password)
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT * FROM Users WHERE UserName='" + username.ToLower() + "' AND Password='" + password + "' ";
        DataTable dt = dal.Query();
        return dt;
    }

    public DataTable getUserdata(string username)
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT * FROM users WHERE Username='" + username + "'";
        DataTable userdata = dal.Query();

        return userdata;
    }

    public DataTable getUserdataByEmail(string email)
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        Database database = new Database();
        DataTable userdata = new DataTable();
        if (isEmailExists(email))
        {
            dal.sqlStatment = "SELECT * FROM users WHERE email='" + email + "'";
            userdata = dal.Query();
        }
        return userdata;
    }

    public bool isAuthorized(string username, string password)
    {
        bool flag = false;
        Utilities utilities = new Utilities();
        username = username.ToLower().Trim();

        bool userExists = isUserExist(username);
        if (userExists)
        {
            DataTable dt = getUserdata(username);
            if (dt.Rows.Count > 0)
            {
                string passwordDB = dt.Rows[0]["Password"].ToString();
                password = utilities.encrypt_pass(username, password);
                if (password.Equals(passwordDB))
                {
                    flag = true;
                }
            }
        }

        return flag;
    }

    public bool insertToken(string userid, string token)
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "INSERT INTO token (UserID, Token) VALUES ('" + userid + "', '" + token + "')";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public bool updatePassword(string userid, string newPassword)
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE users SET Password='" + newPassword + "' WHERE UserID='" + userid + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public bool deleteToken(string userid)
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "DELETE FROM Token where UserID='" + userid + "'";
        if (dal.NonQuery(IsolationLevel.Serializable))
            flag = true;
        return flag;
    }

    public bool tokenExists(string token, string userid)
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT COUNT(*) AS COUNT FROM token WHERE Token='" + token + "' AND UserID='" + userid + "'";
        int count = int.Parse(dal.Query().Rows[0]["COUNT"].ToString());
        if (count > 0)
            flag = true;

        return flag;
    }

    public DataTable getUserAttendance(string userid)
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + userid + "' Order By EventDate Desc, EventTime Desc";
        DataTable userdata = dal.Query();

        return userdata;
    }

    public DataTable getUserAttendanceByDate(string inquiringUsername, string userid, string dateFrom, string dateTo)
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        Database database = new Database();
        if (!(database.isAdmin(database.getUserID(inquiringUsername))))
            userid = inquiringUsername;

        if (dateFrom.Equals("") && dateTo.Equals(""))
            dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + userid + "' Order By EventDate Desc, EventTime Desc";
        else
            if (dateTo.Equals(""))
                dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + userid + "' AND EventDate>='" + "2010-01-01" + "' Order By EventDate Desc, EventTime Desc";
            else
                if (dateFrom.Equals(""))
                    dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + userid + "' AND EventDate<='" + DateTime.Now.ToString("yyyy-MM-dd") + "' Order By EventDate Desc, EventTime Desc";
                else
                    dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + userid + "' AND EventDate>='" + dateFrom + "' AND EventDate<='" + dateTo + "' Order By EventDate Desc, EventTime Desc";

        DataTable userdata = dal.Query();

        return userdata;
    }

    public DataTable getUserAttendanceByDate(string inquiringUsername, List<string> userid, string dateFrom, string dateTo)
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();

        if (isAdmin(getUserID(inquiringUsername)))
        {
            string whereQuery = "";
            if (userid.Count > 1)
            {
                whereQuery = "SELECT * FROM attendancereportall WHERE Userid IN (";
                for (int i = 0; i < userid.Count; i++)
                {
                    whereQuery += "'" + userid[i].ToString() + "'";
                    if (!(userid.Count - i - 1 == 0))
                        whereQuery += ",";
                }
                whereQuery += ")";
                if (dateFrom.Equals("") && dateTo.Equals(""))
                {
                }
                else
                    if (dateTo.Equals(""))
                        dal.sqlStatment = " AND EventDate>='" + "2010-01-01" + "'";
                    else
                        if (dateFrom.Equals(""))
                            dal.sqlStatment = " AND EventDate<='" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                        else
                            dal.sqlStatment = " AND EventDate>='" + dateFrom + "' AND EventDate<='" + dateTo + "'";

                dal.sqlStatment = whereQuery;
            }
            else
            {
                if (userid.Count != 0)
                {
                    if (dateFrom.Equals("") && dateTo.Equals(""))
                        dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + userid[0] + "'";
                    else
                        if (dateTo.Equals(""))
                            dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + userid[0] + "' AND EventDate>='" + "2010-01-01" + "'";
                        else
                            if (dateFrom.Equals(""))
                                dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + userid[0] + "' AND EventDate<='" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                            else
                                dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + userid[0] + "' AND EventDate>='" + dateFrom + "' AND EventDate<='" + dateTo + "'";
                }
                else
                    dal.sqlStatment = "Select * from attendancereportall";
            }
        }
        else
        {
            string inquiringUserID = getUserID(inquiringUsername);
            if (dateFrom.Equals("") && dateTo.Equals(""))
                dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + inquiringUserID + "'";
            else
                if (dateTo.Equals(""))
                    dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + inquiringUserID + "' AND EventDate>='" + "2010-01-01" + "'";
                else
                    if (dateFrom.Equals(""))
                        dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + inquiringUserID + "' AND EventDate<='" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                    else
                        dal.sqlStatment = "SELECT * FROM attendancereportall WHERE Userid='" + inquiringUserID + "' AND EventDate>='" + dateFrom + "' AND EventDate<='" + dateTo + "'";
        }
        dal.sqlStatment += " Order By EventDate Desc, EventTime Desc";

        DataTable userdata = dal.Query();

        return userdata;
    }
   
    public int getUsersCount()//Get number of users
    {
        int number = 0;
        DataTable users = getUsers();
        number = users.Rows.Count;
        return number;
    }

    public string getApplicationLicense()//Get number of users
    {
        string license = "";
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT License FROM Settings";
        license = dal.Query().Rows[0]["License"].ToString();

        return license;
    }

    public bool setApplicationLicense(string applicationLicense)//set number of users.
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE settings SET License='" + applicationLicense + "'";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public DataTable getUsers()//Get user id from users table.
    {
        DataTable usersTemp = null;
        DataTable users = new DataTable("Users");
        Utilities utilities= new Utilities();
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT * FROM users";
        usersTemp = dal.Query();
        users = usersTemp.Clone();
        foreach (DataRow item in usersTemp.Rows)
        {
            string username = item["Username"].ToString();
            string regKey = item["RegKey"].ToString();
            if (utilities.encrypt_pass(username,username).Equals(regKey))
                users.ImportRow(item);
        }
        return users;
    }

    public bool updateAddress(string lat, string lng, string address)
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE attendance SET Address='" + address + "' WHERE GPSLat='" + lat + "' AND GPSLong='" + lng + "'";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }
    //public registerDevice()//Register device
    //{
    //    //user id
    //    //device id

    //    //return boolean
    //}

    //public unregisterDevice()//Unregister device
    //{
    //    //device id

    //    //return boolean
    //}

    //public getStores()//Get stores
    //{
    //    //no input

    //    //return array of stores
    //}

    //public deleteStore($storeID)//Delete store
    //{
    //    //store id

    //    //return boolean
    //}

    //public modifyStore()//Modify store
    //{
    //    //store id
    //    //store name
    //    //store latitude
    //    //store longitude

    //    //return boolean
    //}


    // Functions for Aklny

    public DataTable isHoliday()
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        // check if today is a holiday
        dal.sqlStatment = "SELECT * FROM holidays WHERE HolidayDate like '%'+CONVERT(char(10),getDate(),126 )+'%' ";
        DataTable Holiday = dal.Query();
        return Holiday;

    }

    public DataTable isWeekend()
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT WeekDayName FROM Weekends WHERE WeekDayName = (Select DateNAME (dw,getDate())) ";
        DataTable Weekend = dal.Query();
        return Weekend;
    }

    // get OrderDate
    public DataTable getOrderDate(bool TomorrowOrder)
    {

        string currentDate = DateTime.Now.ToString("yyyy-MM-dd");
        string TodayName = DateTime.Now.DayOfWeek.ToString();
        string OrderDay = "Today";
        if (TomorrowOrder)
        {
            // order for next day
            if (TodayName == "Thursday")
            {
                currentDate = DateTime.Now.AddDays(3).ToString("yyyy-MM-dd");
                OrderDay = DateTime.Now.AddDays(3).DayOfWeek.ToString();
            }
            else if (TodayName == "Friday")
            {
                currentDate = DateTime.Now.AddDays(2).ToString("yyyy-MM-dd");
                OrderDay = DateTime.Now.AddDays(2).DayOfWeek.ToString();
            }
            else
            {
                currentDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                OrderDay = DateTime.Now.AddDays(1).DayOfWeek.ToString();
            }
        }

        DataTable dt = new DataTable();
        DataColumn column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "OrderDate";
        dt.Columns.Add(column);

        DataColumn column2 = new DataColumn();
        column2.DataType = Type.GetType("System.String");
        column2.ColumnName = "OrderDay";
        dt.Columns.Add(column2);

        DataRow row = dt.NewRow();
        row["OrderDate"] = currentDate;
        row["OrderDay"] = OrderDay;

        dt.Rows.Add(row);
        return dt;
    }


    // get Meals

    public DataTable getMeals(string OrderDate, string StartDate)
    {
        // select meals
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT * FROM Meal WHERE ( MealDay = (Select  DateNAME (dw,'" + OrderDate + "')) AND Week=( ABS ( ( Select DatePart(week, '" + OrderDate + "'))  - ( Select DatePart(week, '" + StartDate + "')  )) % (Select MAX(Week) from Meal) + 1) ) " +
            " ORDER BY MealID DESC";

        DataTable dt = dal.Query();
        dal.sqlStatment = "SELECT * FROM Meal WHERE (Week = '0' AND Type ='1') ORDER BY MealID DESC";
        DataTable everyDayDT = dal.Query();

        dt.Merge(everyDayDT);

        return dt;
    }

    // get Locations

    public DataTable getLocations()
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT * FROM Location";
        DataTable Location = dal.Query();
        return Location;
    }

    public DataTable getUserOrder(string userID, string currentDate)
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT * FROM Orders WHERE UserID='" + userID + "' AND MealDate='" + currentDate + "'";
        DataTable Order = dal.Query();
        return Order;
    }


    // check if time is up

    public DataTable checkTimeIsUp (string lastOrder,string TomorrowOrder)
    {
        // check if time is up
        string OrderTime = DateTime.Now.ToString("HH:mm:ss");

        TimeSpan SecondsLeftForToday = DateTime.Parse(lastOrder).Subtract(DateTime.Parse(OrderTime));
        TimeSpan SecondsLeftForTom = DateTime.Parse(OrderTime).Subtract(DateTime.Parse(TomorrowOrder));

        string orderToday = "0";
        string orderTom = "0";
        if (SecondsLeftForToday.TotalSeconds >= 0 )
        {
            orderToday = "1";
        }

        if (SecondsLeftForTom.TotalSeconds >= 0)
        {
            orderTom = "1";
        }

        DataTable dt = new DataTable();
        DataColumn column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "OrderForToday";
        dt.Columns.Add(column);

        DataColumn column2 = new DataColumn();
        column2.DataType = Type.GetType("System.String");
        column2.ColumnName = "OrderForTomorrow";
        dt.Columns.Add(column2);

        DataRow row = dt.NewRow();
        row["OrderForToday"] = orderToday;
        row["OrderForTomorrow"] = orderTom;

        dt.Rows.Add(row);
        return dt;
        
    }

    // Add Order

    public bool addOrder(string userID, string MealID, string LocationID, string currentDate, string Comment, string OptionID)
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "INSERT INTO Orders (UserID,MealID,LocationID,MealDate,Comment,OptionID) VALUES " +
                                " ('" + userID + "','" + MealID + "','" + LocationID + "','" + currentDate + "','" + Comment + "','" + OptionID + "')";
        bool inserted = dal.NonQuery(IsolationLevel.Serializable);
        return inserted;
    }

    public bool editOrder(string userID, string MealID, string LocationID, string currentDate, string Comment, string OptionID)
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "UPDATE Orders SET MealID = '" + MealID + "' ,LocationID = '" + LocationID + "',Comment = '" + Comment + "', OptionID='"+OptionID+"' WHERE MealDate = '" + currentDate + "' AND UserID='" + userID + "' ";
        bool updated = dal.NonQuery(IsolationLevel.Serializable);
        return updated;
    }

    public bool deleteOrder(string userID, string currentDate)
    {
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "DELETE FROM Orders WHERE MealDate = '" + currentDate + "' AND UserID='" + userID + "' ";
        bool deleted = dal.NonQuery(IsolationLevel.Serializable);
        return deleted;
    }

    public bool isAklnyWebLocked(string userID)
    {
        bool Locked = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "SELECT AklnyWebiLock From Users WHERE UserID='"+userID+"' ";
        DataTable dt = dal.Query();
        if(dt.Rows.Count == 1)
        {
            if(dt.Rows[0][0].ToString()=="1")
            {
                Locked = true;
            }
        }
        return Locked;
    }

    
}