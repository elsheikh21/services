﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace ECS_Framework
{
    /// <summary>
    /// Database layer manipulation class.
    /// </summary>
    public class MSSQLDataAccessLayer
    {
        public string dataSource { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string initialCatalog { get; set; }
        public SqlConnection MSSQLConn { get; set; }
        public SqlCommand MSSQLComm { get; set; }
        public string sqlStatment { get; set; }

        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="dataSource">The IP or hostname to connect to the database.</param>
        /// <param name="username">The database username.</param>
        /// <param name="password">The database password.</param>
        /// <param name="initialCatalog">The database to connect to.</param>
        public MSSQLDataAccessLayer(string dataSource, string username, string password, string initialCatalog)
        {
            this.dataSource = dataSource;
            this.username = username;
            this.password = password;
            this.initialCatalog = initialCatalog;
            this.MSSQLConn = new SqlConnection();
            if (username == "" && password == "")
            {
                this.MSSQLConn.ConnectionString = String.Format("Initial Catalog={1};Data Source={2}; ", this.initialCatalog, this.dataSource);
            }
            else
            {
                this.MSSQLConn.ConnectionString = String.Format("User ID={0};Initial Catalog={1};Data Source={2}; password={3}", this.username, this.initialCatalog, this.dataSource, this.password);
            }
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="dataSource">The IP or hostname to connect to the database.</param>
        /// <param name="username">The database username.</param>
        /// <param name="password">The database password.</param>
        /// <param name="initialCatalog">The database to connect to.</param>
        public MSSQLDataAccessLayer()
        {
            this.dataSource = ConfigurationManager.AppSettings.Get("MSSQLDataSource");
            this.username = ConfigurationManager.AppSettings.Get("MSSQLUsername");
            this.password = ConfigurationManager.AppSettings.Get("MSSQLPassword");
            this.initialCatalog = ConfigurationManager.AppSettings.Get("MSSQLInitialCatalog");
            this.MSSQLConn = new SqlConnection();

            if (this.username == "" && this.password == "")
            {
                this.MSSQLConn.ConnectionString = String.Format("Integrated Security=True;Initial Catalog={0};Data Source={1}", this.initialCatalog, this.dataSource);
            }
            else
             this.MSSQLConn.ConnectionString = String.Format("User ID={0};Initial Catalog={1};Data Source={2}; password={3}", this.username, this.initialCatalog, this.dataSource, this.password);
        }

        /// <summary>
        /// Opens a database connection.
        /// </summary>
        public bool OpenConnection()
        {
            try
            {
                if (this.MSSQLConn.State == ConnectionState.Closed)
                    this.MSSQLConn.Open();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                return false;
            }

            return true;
        }

        /// <summary>
        /// Closes an open database connection if opened.
        /// </summary>
        public void closeConnection()
        {
            try
            {
                if (this.MSSQLConn.State == ConnectionState.Open)
                    this.MSSQLConn.Close();
            }
            catch (Exception ex)
            {
                //log exception
            }
        }

        /// <summary>
        /// Select from database method.
        /// </summary>
        public DataTable Query()
        {
            DataTable dt = new DataTable();
            try
            {
                if(this.OpenConnection())
                {
                    this.MSSQLComm = new SqlCommand();
                    this.MSSQLComm.CommandText = this.sqlStatment;
                    this.MSSQLComm.Connection = this.MSSQLConn;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = this.MSSQLComm;
                    da.Fill(dt);
                }

                

            }
            catch (SqlException ex)
            {
                //log exception
            }
            catch (Exception ex)
            {
                //log exception
                //EventLog log = new EventLog(LogSource.Database, LogType.Exception, "", ex.ToString());
                //log.writeLog();
            }
            finally
            {
                this.closeConnection();
            }
            return dt;
        }

        //public DataTable Query()
        //{
        //    DataTable dt = new DataTable();
        //    try
        //    {
        //        this.OpenConnection();
        //        this.MSSQLComm = new SqlCommand();
        //        this.MSSQLComm.CommandText = this.sqlStatment;
        //        this.MSSQLComm.Connection = this.MSSQLConn;
        //        SqlDataAdapter da = new SqlDataAdapter();
        //        da.SelectCommand = this.MSSQLComm;
        //        da.Fill(dt);
        //    }
        //    catch (Exception ex)
        //    {
        //        //log exception
        //        //EventLog log = new EventLog(LogSource.Database, LogType.Exception, "", ex.ToString());
        //        //log.writeLog();
        //    }
        //    finally
        //    {
        //        this.closeConnection();
        //    }
        //    return dt;
        //}

        /// <summary>
        /// Insert to database method.
        /// </summary>
        /// <returns></returns>
        public Boolean NonQuery(IsolationLevel isolationLevel)
        {
            Boolean flag = false;
            try
            {
                if (this.OpenConnection())
                {
                    this.MSSQLComm = new SqlCommand();
                    this.MSSQLComm.Transaction = this.MSSQLConn.BeginTransaction(isolationLevel);
                    this.MSSQLComm.CommandText = this.sqlStatment;
                    this.MSSQLComm.Connection = this.MSSQLConn;
                    this.MSSQLComm.ExecuteNonQuery();
                    this.MSSQLComm.Transaction.Commit();
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            catch (SqlException ex)
            {
                //log exception
                this.MSSQLComm.Transaction.Rollback();
                return flag;
            }
            finally
            {
                this.closeConnection();
            }
            return flag;
        }

        public Boolean NonQueryWithParamter(IsolationLevel isolationLevel,byte [] parm)
        {
            Boolean flag = false;
            try
            {
                if (this.OpenConnection())
                {
                    this.MSSQLComm = new SqlCommand();
                    this.MSSQLComm.Parameters.Add("@param", SqlDbType.VarBinary, parm.Length).Value = parm;
                    this.MSSQLComm.Transaction = this.MSSQLConn.BeginTransaction(isolationLevel);
                    this.MSSQLComm.CommandText = this.sqlStatment;
                    this.MSSQLComm.Connection = this.MSSQLConn;
                    this.MSSQLComm.ExecuteNonQuery();
                    this.MSSQLComm.Transaction.Commit();
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            catch (SqlException ex)
            {
                //log exception
                this.MSSQLComm.Transaction.Rollback();
                return flag;
            }
            finally
            {
                this.closeConnection();
            }
            return flag;
        }


    }
}