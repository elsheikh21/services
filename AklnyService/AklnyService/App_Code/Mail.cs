﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Data;
using System.Collections.Specialized;
using System.Net;
using System.Configuration;
using System.Web;

namespace ECS_Framework
{
    /// <summary>
    /// Mail Class.
    /// </summary>
    public class Mail: MailMessage
    {
        public SmtpClient smtpServer { get; set; }
        public MailMessage mail { get; set; }
        public int port { get; set; }
        public string smtpServerName { get; set; }
        private NetworkCredential credentials { get; set; }
        public Boolean isSSL { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Mail()
        {
            try
            {
                this.port = int.Parse(ConfigurationManager.AppSettings.Get("Port"));
                this.smtpServer = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"));
                this.credentials = new NetworkCredential();
                this.credentials.UserName = ConfigurationManager.AppSettings.Get("SMTPUserName");
                this.credentials.Password = ConfigurationManager.AppSettings.Get("SMTPPassword");
                this.smtpServer.Credentials = this.credentials;
                this.isSSL = Boolean.Parse(ConfigurationManager.AppSettings.Get("SSL"));

                this.mail = new MailMessage();
                this.mail.From = new MailAddress(this.credentials.UserName, ConfigurationManager.AppSettings.Get("FromName"));
                //this.mail.To.Add(new MailAddress(ConfigurationManager.AppSettings.Get("To")));
                //this.mail.Bcc.Add(new MailAddress(ConfigurationManager.AppSettings.Get("Bcc")));
                //this.mail.CC.Add(new MailAddress(ConfigurationManager.AppSettings.Get("CC")));
                this.mail.Subject = ConfigurationManager.AppSettings.Get("Subject");
                this.mail.IsBodyHtml = Boolean.Parse(ConfigurationManager.AppSettings.Get("isBodyHtml"));

                
            }
            catch (Exception ex)
            {
                //log exception
            }
        }

        /// <summary>
        /// Send an email.
        /// </summary>
        /// <returns></returns>
        public Boolean Send()
        {
            Boolean flag = false;
            try
            {
                //string link = "<a href=\"" + ConfigurationManager.AppSettings.Get("Website") + "\">" + ConfigurationManager.AppSettings.Get("Company") + "</a>";
                //this.mail.Body = ConfigurationManager.AppSettings.Get("Body");
                //this.mail.Body += "<div ><img src=\"cid:imageId\" style=\"width:50px; padding-left:10\">";
               // this.mail.Body += ConfigurationManager.AppSettings.Get("Signature") + " " + link + "</div>";

                //AlternateView htmlView = AlternateView.CreateAlternateViewFromString(this.mail.Body, null, "text/html");

                //LinkedResource imagelink = new LinkedResource(HttpContext.Current.Server.MapPath("~/Assets/images/ECSLogo.png"), "image/png");

                //imagelink.ContentId = "imageId";

                //imagelink.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                //htmlView.LinkedResources.Add(imagelink);
                //this.mail.AlternateViews.Add(htmlView);
                this.smtpServer.Send(this.mail);
                flag = true;
            }
            catch (Exception)
            {
                //log exception
            }
            return flag;
        }

        public Boolean Send(string bodyString)
        {
            Boolean flag = false;
            try
            {
                this.mail.Subject = "Reset your password on www.ecs-co.com";


                string bodytext = "<div style=\"color:Black\"><span style=\"font-weight:bold\">Dear Ms.\\Mr. ,</span>";
                bodytext += "<br /><br />You asked us to reset your password for <span style=\"font-weight:bold\">ecs-co.com</span> account tied to this e-mail address.";
                bodytext += "<br /><br />If you did not make this request, please ignore this email.";
                bodytext += "<br /><br />If you <span style=\"font-style:italic; font-weight:bold\">did</span> make this request, ";
                bodytext += "<a href=\"www.ecs-co.com/website/ResetPass\">Please Click Here</a> to change your password.";
                bodytext += "<br /><br /><span style=\"font-weight:bold\">Your token is:</span> " + bodyString;
                
                bodytext += "<br /><br />";

                bodytext += "Best regards, ";

                bodytext += "<br /><br />";

                bodytext += "Enterprise Consultancy Services (ECS)";

                bodytext += "<br /><br />";

                bodytext += "<span style=\"color:#31849b !important\">Phone</span> +202 2 6344 165 | <span style=\"color:#31849b !important\">Fax</span> +202 26 366 613";

                bodytext += "<br />";

                bodytext += "<span style=\"color:#31849b !important\">Address</span> 111 Farid Semeka St., Heliopolis, Cairo, Egypt.";

                bodytext += "<br /></div>";

                this.mail.Body = bodytext;
                //string link = "<a href=\"" + ConfigurationManager.AppSettings.Get("Website") + "\">" + ConfigurationManager.AppSettings.Get("Company") + "</a>";
                //this.mail.Body = ConfigurationManager.AppSettings.Get("Body");
                this.mail.Body += "<div ><img src=\"cid:imageId\" style=\"width:350px; padding-left:10\">";
                this.mail.Body += "</div>";

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(this.mail.Body, null, "text/html");

                LinkedResource imagelink = new LinkedResource(HttpContext.Current.Server.MapPath("~/Assets/images/signatureLogo.png"), "image/png");

                imagelink.ContentId = "imageId";

                imagelink.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                htmlView.LinkedResources.Add(imagelink);
                this.mail.AlternateViews.Add(htmlView);
                this.smtpServer.Send(this.mail);
                flag = true;
            }
            catch (Exception)
            {
                //log exception
            }
            return flag;
        }

        public bool Send(DataTable records)// Sends an email to the person incharge of the attendance machines.
        {
            Boolean flag = false;
            // try
            //{
            //string[] mailSettings = getMail(); //Get mail settings from Mail.xml.
            //  if (mailSettings != null)
            //   {
            //       SmtpClient SmtpServer = new SmtpClient(mailSettings[3].ToString());

            //       mail.From = new MailAddress(mailSettings[0].ToString(), mailSettings[8].ToString());
            //       mail.Subject = mailSettings[6].ToString();
            //       mail.To.Add(mailSettings[5].ToString());
            //       mail.Body = "<html><body>" + mailSettings[7].ToString() + "</br></br>";
            //       foreach (DataRow item in records.Rows)
            //{
            //    string lat = item["GPSLat"].ToString();
            //    string lon = item["GPSLong"].ToString();
            //    string latlong = lat + "," + lon;
            //    string table = "<table border id='loginTable'><tbody><tr><td>Employee</td> <td>" + item["Name"].ToString() + "</td><td rowspan=6>	<img src='http://maps.googleapis.com/maps/api/staticmap?center=" + latlong + "&zoom=16&size=320x220&sensor=false&maptype=roadmap&markers=color:blue%7Clabel:C%7C" + latlong + "'></td></tr><tr><td>Latitude</td> <td>" + lat + "</td></tr><tr><td>Longitude</td> <td>" + lon + "</td></tr><tr><td>Time</td> <td>" + item["EventTime"].ToString() + "</td></tr><tr><td>Event Type</td> <td>" + item["EventType"].ToString() + "</td></tr><tr><td></td></tr></tbody></table>";

            //    mail.Body += table + "</br></br>";
            //}
            //mail.Body += "</body></html>";


            //          SmtpServer.Port = Int16.Parse(mailSettings[2].ToString());
            //          SmtpServer.Credentials = new System.Net.NetworkCredential(mailSettings[0].ToString(), mailSettings[1].ToString());
            //        SmtpServer.EnableSsl = Convert.ToBoolean(mailSettings[4].ToString());

            //        SmtpServer.Send(mail);

            return flag;
            //   }
            //  else
            //  {
            //      return false;
            //  }
            //  }
            //catch (Exception ex)
            //{
            //    //ADD ERROR TO LOG
            //    return false;
            //}
        }

        public Boolean SendForgetPass(string UserName,string token,string URL,string email)
        {
            Boolean flag = false;
            try
            {
                //----- Message Body system ----//
                string bodytext_System = "<div style=\"color:Black;font-family: arial;font-size:12px\">";
                bodytext_System += "<div style=\"float:left;width:100%;\">";
                bodytext_System += "<img src=\"cid:imageId\" />";
                bodytext_System += "</div>";
                bodytext_System += "<div style=\"margin-left:20px;margin-top:10px;float:left;width:100%\">";
                bodytext_System += " Dear " + UserName + ",";
                bodytext_System += "<br /><br />Your password has been reset. Your token is below.";
                bodytext_System += "<br /><br /><span style=\"font-weight:bold\">Token:</span> " + token;
                bodytext_System += "<br /><br />To Login <a href=\""+URL+"/Home#Forms/0/\" >Click here</a>.";
                bodytext_System += "<br /><br />";
                bodytext_System += "Best regards, ";
                bodytext_System += "<br />";
                bodytext_System += "</div>";
                bodytext_System += "</div>";

                LinkedResource imagelink = new LinkedResource(HttpContext.Current.Server.MapPath("~/Assets/imgs/backGroundPassReset.png"), "image/png");
                imagelink.ContentId = "imageId";
                imagelink.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;

                
                this.mail.Body = bodytext_System;

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(this.mail.Body, null, "text/html");
                htmlView.LinkedResources.Add(imagelink);
                this.mail.AlternateViews.Add(htmlView);
                this.IsBodyHtml = true;
                this.mail.Subject = "Reset Password";
                //this.mail.To.Add(new MailAddress(email));
                this.smtpServer.Send(this.mail);
                flag = true;
            }
            catch (Exception)
            {

            }
            return flag;
        }
    }
}


