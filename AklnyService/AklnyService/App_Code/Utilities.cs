﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
//using System.DirectoryServices.AccountManagement;
//using System.DirectoryServices.ActiveDirectory;
//using System.DirectoryServices;
using System.IO;
using System.Text;
//using MobittendService;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Security.Cryptography;
using ECS_Framework;
/// <summary>
/// Summary description for Functions
/// </summary>


public class Utilities
{
    public Utilities()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public String getContact(String eid)
    {

        //SqlConnection conn;
        //conn = ConnectionManager.GetConnection();
        //conn.Open();

        //SqlCommand newCmd = conn.CreateCommand();

        //newCmd.CommandType = CommandType.Text;
        //newCmd.CommandText = "select Name from dbo.StudentContact where studentNo='" + eid + "'";
        //SqlDataReader sdr = newCmd.ExecuteReader();
        //String address = null;
        //if (sdr.Read())
        //{
        //    address = sdr.GetValue(0).ToString();
        //}
        //conn.Close();

        //return address;
        return "working";
    }

    public string EmployeeNumber(string userID)//Formats the employee number(MAX Length 8).
    {
        if (userID.Length > 0)
        {
            if (userID.Length == 1)
                return "0000000" + userID;
            else if (userID.Length == 2)
                return "000000" + userID;
            else if (userID.Length == 3)
                return "00000" + userID;
            else if (userID.Length == 4)
                return "0000" + userID;
            else if (userID.Length == 5)
                return "000" + userID;
            else if (userID.Length == 6)
                return "00" + userID;
            else if (userID.Length == 7)
                return "0" + userID;
            else if (userID.Length == 8)
                return userID;
            else return "";
        }
        else return "";
    }

    public string MachineNumber(string machineNumber)//Formats the machine number(MAX Length 4).
    {
        if (machineNumber.Length > 0)
        {
            if (machineNumber.Length == 1)
                return "000" + machineNumber;
            else if (machineNumber.Length == 2)
                return "00" + machineNumber;
            else if (machineNumber.Length == 3)
                return "0" + machineNumber;
            else return "";
        }
        else
        {
            return "";
        }
    }

    public string EventType(string Event)//Formats the Event type.
    {
        string mode = "";
        switch (Event)
        {
            case "Check In":
                mode = "Check In      ";
                break;

            case "Check Out":
                mode = "Check Out     ";
                break;
        }
        return mode;
    }

    public string randomPassword(int length)//Generates random passwords.
    {
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        string result = new string(
            Enumerable.Repeat(chars, length)
                      .Select(s => s[random.Next(s.Length)])
                      .ToArray());
        return result;
    }

    public string PasswordGenerator(int passwordLength, bool strongPassword)
    {
        Random Random = new Random();
        int seed = Random.Next(1, int.MaxValue);
        //const string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
        const string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
        const string specialCharacters = @"!#$%&'()*+,-./:;<=>?@[\]_";

        var chars = new char[passwordLength];
        var rd = new Random(seed);

        for (var i = 0; i < passwordLength; i++)
        {
            // If we are to use special characters
            if (strongPassword && i % Random.Next(3, passwordLength) == 0)
            {
                chars[i] = specialCharacters[rd.Next(0, specialCharacters.Length)];
            }
            else
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
        }

        return new string(chars);
    }

    public bool writeAttendanceToDatabase(string terminalID, string userid, string timeEventType, string datewithdash9, string timeFormated, string GPSLat, string GPSLong, string ATTAB, string IMEI, string simSerial, string comment, string isOffline, string address)//Insert attendance record in attendance table. 
    {
        bool flag = false;
        MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
        dal.sqlStatment = "INSERT INTO attendance (TerminalID, UserID, EventType, EventDate, EventTime, GPSLat, GPSLong,ATTAB,IMEI,SimSerialNumber,Comment,isOffline,Address)" +
                            "VALUES ('" + terminalID + "', '" + userid + "', '" + timeEventType + "', '" + datewithdash9 + "', '" + timeFormated + "','" + GPSLat + "','" + GPSLong + "','" + ATTAB + "','" + IMEI + "','" + simSerial + "','" + comment + "','" + isOffline + "','" + address + "')";
        flag = dal.NonQuery(IsolationLevel.Serializable);
        return flag;
    }

    public bool writeAttendanceToFile(string filePath, string fileName, string terminalID, string userid, string timeEventType, string date, string time, string ATTAB)//Write attendance record in attendance file.
    {
        bool flag = false;
        terminalID = MachineNumber(terminalID);
        userid = EmployeeNumber(userid);
        timeEventType = EventType(timeEventType);
        if (ATTAB.Equals("0000"))
            ATTAB = "";

        flag = WriteToFile(filePath, fileName, terminalID + "  " + userid + "  " + timeEventType + "  " + date + "  " + time + "  " + ATTAB);

        return flag;
    }

    public bool WriteToFile(string filePath,string fileName, string content)
    {
        Boolean flag = false;
        try
        {
            checkDirectoryExistance(filePath);
            FileStream fileStream;
            fileStream = File.Open(filePath + "\\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
            fileStream.Close();
            StreamWriter streamWriter = new StreamWriter(filePath + "\\" + fileName, true, Encoding.UTF8);
            streamWriter.WriteLine(content);
            streamWriter.Close();
            flag = true;
        }
        catch (Exception ex)
        {
            //log exception

        }

        return flag;
    }

    public bool isInt(string txt)
    {
        int f;

        if (Int32.TryParse(txt, out f))
            return true;
        else
            return false;
    }

    public string encrypt_pass(string user, string pass)
    {
        UTF8Encoding textConverter = new UTF8Encoding();
        byte[] passBytes = textConverter.GetBytes("mobittend" + user + "webittend" + pass);
        passBytes = new SHA384Managed().ComputeHash(passBytes);
        string s = "";
        for (int i = 0; i < passBytes.Length; i++)
        {
            s = s + (passBytes[i] + i).ToString();
        }
        int ii = s.Length;
        if (ii > 300)
        {
            ii = 300;
        }
        return s.Substring(0, ii - 1);
    }

    public bool IsValidEmail(string email)
    {
        try
        {
            var addr = new System.Net.Mail.MailAddress(email);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public string encryptX(int number)
    {
        StringBuilder x = new StringBuilder("KFCGJPWXZQTRYLTWPFGFJWKXZ");

        for (int i = 0; i < number.ToString().Length; i++)
        {
            switch (Int16.Parse((number.ToString())[i].ToString()))
            {

                case 1:
                    x[i] = 'M';
                    break;
                case 2:
                    x[i] = 'O';
                    break;
                case 3:
                    x[i] = 'H';
                    break;
                case 4:
                    x[i] = 'A';
                    break;
                case 5:
                    x[i] = 'E';
                    break;
                case 6:
                    x[i] = 'D';
                    break;
                case 7:
                    x[i] = 'S';
                    break;
                case 8:
                    x[i] = 'N';
                    break;
                case 9:
                    x[i] = 'I';
                    break;
                case 0:
                    x[i] = 'B';
                    break;
                default:
                    break;
            }
        }
        return x.ToString();
    }

    public string decryptX(string number)
    {
        StringBuilder x = new StringBuilder("KFCGJPWXZQTRYLTWPFGFJWKXZ");
        string s = "";
        for (int i = 0; i < number.ToString().Length; i++)
        {
            switch ((number.ToString())[i])
            {
                case 'M':
                    x[i] = '1';
                    break;
                case 'O':
                    x[i] = '2';
                    break;
                case 'H':
                    x[i] = '3';
                    break;
                case 'A':
                    x[i] = '4';
                    break;
                case 'E':
                    x[i] = '5';
                    break;
                case 'D':
                    x[i] = '6';
                    break;
                case 'S':
                    x[i] = '7';
                    break;
                case 'N':
                    x[i] = '8';
                    break;
                case 'I':
                    x[i] = '9';
                    break;
                case 'B':
                    x[i] = '0';
                    break;
                // case 'X':
                default:
                    s = x.ToString().Substring(0, i);
                    return s;
                // default:
                //    break;
            }
        }
        return s;
    }

    private bool checkDirectoryExistance(string DirectoryName)
    {
        bool flag = false;
        if (!Directory.Exists(DirectoryName))
        {
            Directory.CreateDirectory(DirectoryName);
            flag = true;
        }
        return flag;
    }

    public string toEnglishNumbers(string arabic)
    {
        string english = "";
        foreach (char item in arabic.ToCharArray())
        {
            if (item.Equals(' '))
                english += ' ';
            else
                english += char.GetNumericValue(item);
        }
        return english;

    }
}