﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Net.Mail;
using System.IO;
using ECS_Framework;
using System.Web;
using System.Threading;

namespace AklnyService {
    public partial class Service1:ServiceBase {
        private ManualResetEvent _shutdownEvent = new ManualResetEvent(false);
        private Thread _thread;
        public Service1() {
            InitializeComponent();
        }

        protected override void OnStart(string[] args) {
            _thread = new Thread(ThreadFun);
            _thread.Name = "My Worker Thread";
            _thread.IsBackground = true;
            _thread.Start();
        }

        protected override void OnStop() {
            _shutdownEvent.Set();
            if( !_thread.Join(3000) ) { // give the thread 3 seconds to stop
                _thread.Abort();
            }
        }

        public void ThreadFun() {
            bool notifired = false;
            bool fired = false;
            while( !_shutdownEvent.WaitOne(0) ) {
                if( DateTime.Now.Hour == 9 && DateTime.Now.Minute == 30 ) {
                    if( !notifired ) {
                        notifired = true;
                        sendNotiEmail();
                    }
                } else if( DateTime.Now.Hour == 10 && DateTime.Now.Minute == 5 ) {
                    if( fired == false ) {
                        // do your work here
                        fired = true;
                        SendEmail_AR();
                    }
                } else {
                    notifired = false;
                    fired = false;
                }
                System.Threading.Thread.Sleep(1000);
            }


        }

        public void sendNotiEmail() {
            Database database = new Database(); // creating new database
            DataTable Holiday = database.isHoliday(); // checking for holidays
            try {
                if( Holiday.Rows.Count == 0 ) { // if not holiday
                    DataTable Weekend = database.isWeekend();
                    if( Weekend.Rows.Count == 0 ) { // if not weekend
                        DataTable settingsDT = database.getSettings();
                        if( settingsDT.Rows.Count == 1 ) {
                            string currentDate = DateTime.Now.ToString("yyyy-MM-dd"); // getting current date
                            
                            MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
                            // criteria needed
                            dal.sqlStatment = "SELECT email FROM users WHERE UserID NOT IN( SELECT UserID FROM Orders WHERE MealDate ='" + currentDate + "') AND WebiLock = 0 AND UserID != 1"; 
                            DataTable notOrdered = dal.Query(); // executing the query

                            // creating new instance of ecs framework email
                            ECS_Framework.Mail mail = new ECS_Framework.Mail();
                            //setting the email subject
                            mail.mail.Subject = "ECS Order Notification ❤️.";
                            // Body can use html tags
                            mail.mail.IsBodyHtml = true;
                            // setting the email body
                            mail.mail.Body = "<div style='font-family:Calibri'> Please place your lunch order, before 10 AM.<br/> Login <a href=\"http://ecsservices.ecs-co.com:8080/aklny/Home\"> here </a> <br/> Thank you and enjoy your meal. </div>";
                            
                            for( int i = 0 ; i < notOrdered.Rows.Count ; i++ ) { // loop on the datatable rows to get emails
                                mail.mail.To.Clear();
                                mail.mail.To.Add(new MailAddress(notOrdered.Rows[i][0].ToString())); // create new instance of email address and add it
                                mail.Send(); // send the email
                                Thread.Sleep(1000); // allow thread to sleep in order to prevent crashing, or sending duplicate mails
                            }

                        }
                    }
                }
            } catch( Exception e ) {
                string msg = e.Message;
                ECS_Framework.Mail mail = new ECS_Framework.Mail();
                mail.mail.To.Add(new MailAddress("ahmed.elsheikh@ecs-co.com"));
                mail.mail.IsBodyHtml = true;
                mail.mail.Body = String.Format("An error was encountered while sending lunch notifications, the exception is thrown due to {0}", msg);
                mail.mail.Subject = "Error sending lunch notifications";
                mail.Send();
            }
        }

        public void SendEmail_EN() {
            // create Mail Body
            string FaridMealBody = "";
            string OsmanMealBody = "";
            string currentDate = DateTime.Now.ToString("yyyy-MM-dd");

            // get meals
            Database database = new Database();

            // check if today is a holiday
            DataTable Holiday = database.isHoliday();

            try {
                if( Holiday.Rows.Count == 0 ) {
                    // check if today is a weekend
                    DataTable Weekend = database.isWeekend();
                    if( Weekend.Rows.Count == 0 ) {
                        DataTable settingsDT = database.getSettings();
                        if( settingsDT.Rows.Count == 1 ) {
                            // get meals
                            string strartDate = settingsDT.Rows[0]["StartDate"].ToString();
                            DataTable MealDT = database.getMeals(currentDate, strartDate);

                            MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
                            // Summary Farid
                            string NormalMealID = "";

                            if( MealDT.Rows.Count > 0 ) {
                                NormalMealID = MealDT.Rows[0]["MealID"].ToString();
                            }


                            // Summary Farid

                            // Meals without options
                            dal.sqlStatment = "SELECT ord.MealID,ord.OptionID,COUNT(ord.MealID) AS Count ,me.MealName FROM Orders ord INNER JOIN Meal me ON ord.MealID=me.MealID WHERE ord.MealDate ='" + currentDate + "'  AND ord.LocationID='1' AND (ord.OptionID IS NULL OR ord.OptionID ='' ) GROUP BY ord.MealID,ord.optionID,me.MealName ";
                            DataTable SummaryMealDt_Farid = dal.Query();

                            // get office meals 
                            dal.sqlStatment = "SELECT ofmeal.MealID,ofmeal.Count,me.MealName FROM Office_Meals ofmeal INNER JOIN Meal me ON ofmeal.MealID=me.MealID WHERE ofmeal.Day = (Select  DateNAME (dw,'" + currentDate + "')) AND ofmeal.LocationID='1' ";
                            DataTable OfficeMealsDt_Farid = dal.Query();
                            if( OfficeMealsDt_Farid.Rows.Count > 0 ) {
                                for( int i = 0 ; i < OfficeMealsDt_Farid.Rows.Count ; i++ ) {

                                    // add count to Summary meal if mealid exist
                                    DataRow[] foundRows = SummaryMealDt_Farid.Select("MealID =" + OfficeMealsDt_Farid.Rows[i]["MealID"].ToString());
                                    if( foundRows.Length == 1 ) {
                                        foundRows[0]["Count"] = "" + (int.Parse(foundRows[0]["Count"].ToString()) + int.Parse(OfficeMealsDt_Farid.Rows[i]["Count"].ToString()));
                                    } else {
                                        // doesn't exist so add it
                                        DataRow Newrow = SummaryMealDt_Farid.NewRow();
                                        Newrow["MealName"] = OfficeMealsDt_Farid.Rows[i]["MealName"].ToString();
                                        Newrow["Count"] = OfficeMealsDt_Farid.Rows[i]["Count"].ToString();
                                        Newrow["MealID"] = OfficeMealsDt_Farid.Rows[i]["MealID"].ToString();
                                        SummaryMealDt_Farid.Rows.Add(Newrow);
                                    }
                                }
                            }

                            // Meals with options
                            dal.sqlStatment = "SELECT ord.MealID,OptionID,Count(OptionID) AS Count ,me.MealName FROM Orders ord INNER JOIN Meal me ON ord.MealID=me.MealID WHERE ord.OptionID !='' AND  MealDate ='" + currentDate + "'  AND ord.LocationID='1' group by ord.MealID,me.MealName, OptionID having count(*) >= 1";
                            DataTable FaridOptionMeals = dal.Query();
                            for( int i = 0 ; i < FaridOptionMeals.Rows.Count ; i++ ) {
                                if( FaridOptionMeals.Rows[i]["OptionID"].ToString() != "" ) {
                                    dal.sqlStatment = "SELECT p1.MealID,STUFF(( SELECT ', '+OptionText FROM  MealOptions p2 WHERE p2.MealID = p1.MealID AND p2.OptionID IN (" + FaridOptionMeals.Rows[i]["OptionID"].ToString() + ") ORDER BY OptionText FOR XML PATH('') ),1,1, '') AS Options FROM MealOptions p1 WHERE MealID = '" + FaridOptionMeals.Rows[i]["MealID"].ToString() + "' GROUP BY MealID ";
                                    DataTable optionDt = dal.Query();
                                    if( optionDt.Rows.Count == 1 ) {
                                        FaridOptionMeals.Rows[i]["MealName"] = FaridOptionMeals.Rows[i]["MealName"] + " (" + optionDt.Rows[0]["Options"].ToString().TrimStart(' ') + ")";
                                    }
                                }
                            }

                            SummaryMealDt_Farid.Merge(FaridOptionMeals);
                            SummaryMealDt_Farid.DefaultView.Sort = "MealID DESC";
                            SummaryMealDt_Farid = SummaryMealDt_Farid.DefaultView.ToTable();
                            if( SummaryMealDt_Farid.Rows.Count > 0 ) {
                                // check if normal meal exist
                                //DataRow[] Row = SummaryMealDt_Farid.Select("MealID = " + NormalMealID + "");

                                //if (Row.Length == 0)
                                //{
                                //    DataRow Newrow = SummaryMealDt_Farid.NewRow();
                                //    Newrow["MealName"] = MealDT.Rows[0]["MealName"].ToString();
                                //    Newrow["Count"] = "2";
                                //    Newrow["MealID"] = NormalMealID;
                                //    SummaryMealDt_Farid.Rows.Add(Newrow);
                                //}
                                for( int i = 0 ; i < SummaryMealDt_Farid.Rows.Count ; i++ ) {
                                    FaridMealBody += "<tr>";
                                    FaridMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:0;\">";
                                    FaridMealBody += SummaryMealDt_Farid.Rows[i]["MealName"];
                                    FaridMealBody += "</td>";
                                    FaridMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:22px; padding-bottom:9px; padding-right:0;text-align:left\">";
                                    FaridMealBody += SummaryMealDt_Farid.Rows[i]["Count"];
                                    FaridMealBody += "</td>";
                                    FaridMealBody += "</tr>";
                                }
                            } else {
                                FaridMealBody += "<tr>";
                                FaridMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:0;\">";
                                FaridMealBody += " - ";
                                FaridMealBody += "</td>";
                                FaridMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:22px; padding-bottom:9px; padding-right:0;text-align:left\">";
                                FaridMealBody += " - ";
                                FaridMealBody += "</td>";
                                FaridMealBody += "</tr>";
                            }


                            // Summary Osman

                            // Meals without options
                            //dal.sqlStatment = "SELECT ord.MealID,ord.OptionID,CASE WHEN ord.MealID ='" + NormalMealID + "' THEN COUNT(ord.MealID) + 2 ELSE COUNT(ord.MealID) END  AS Count ,me.MealName FROM Orders ord INNER JOIN Meal me ON ord.MealID=me.MealID WHERE ord.MealDate ='" + currentDate + "'  AND ord.LocationID='2' AND (ord.OptionID IS NULL OR ord.OptionID ='' ) GROUP BY ord.MealID,ord.optionID,me.MealName ";
                            dal.sqlStatment = "SELECT ord.MealID,ord.OptionID,COUNT(ord.MealID) AS Count ,me.MealName FROM Orders ord INNER JOIN Meal me ON ord.MealID=me.MealID WHERE ord.MealDate ='" + currentDate + "'  AND ord.LocationID='2' AND (ord.OptionID IS NULL OR ord.OptionID ='' ) GROUP BY ord.MealID,ord.optionID,me.MealName ";
                            DataTable SummaryMealDt_Osam = dal.Query();


                            // get office meals 
                            dal.sqlStatment = "SELECT ofmeal.MealID,ofmeal.Count,me.MealName FROM Office_Meals ofmeal INNER JOIN Meal me ON ofmeal.MealID=me.MealID WHERE ofmeal.Day = (Select  DateNAME (dw,'" + currentDate + "')) AND ofmeal.LocationID='2' ";
                            DataTable OfficeMealsDt_Osman = dal.Query();
                            if( OfficeMealsDt_Osman.Rows.Count > 0 ) {
                                for( int i = 0 ; i < OfficeMealsDt_Osman.Rows.Count ; i++ ) {

                                    // add count to Summary meal if mealid exist
                                    DataRow[] foundRows = SummaryMealDt_Osam.Select("MealID =" + OfficeMealsDt_Osman.Rows[i]["MealID"].ToString());
                                    if( foundRows.Length == 1 ) {
                                        foundRows[0]["Count"] = "" + (int.Parse(foundRows[0]["Count"].ToString()) + int.Parse(OfficeMealsDt_Osman.Rows[i]["Count"].ToString()));
                                    } else {
                                        // doesn't exist so add it
                                        DataRow Newrow = SummaryMealDt_Osam.NewRow();
                                        Newrow["MealName"] = OfficeMealsDt_Osman.Rows[i]["MealName"].ToString();
                                        Newrow["Count"] = OfficeMealsDt_Osman.Rows[i]["Count"].ToString();
                                        Newrow["MealID"] = OfficeMealsDt_Osman.Rows[i]["MealID"].ToString();
                                        SummaryMealDt_Osam.Rows.Add(Newrow);
                                    }
                                }
                            }

                            // Meals with options
                            dal.sqlStatment = "SELECT ord.MealID,OptionID,Count(OptionID) AS Count ,me.MealName FROM Orders ord INNER JOIN Meal me ON ord.MealID=me.MealID WHERE ord.OptionID !='' AND  MealDate ='" + currentDate + "'  AND ord.LocationID='2' group by ord.MealID,me.MealName, OptionID having count(*) >= 1";
                            DataTable OsmanOptionMeals = dal.Query();
                            for( int i = 0 ; i < OsmanOptionMeals.Rows.Count ; i++ ) {
                                if( OsmanOptionMeals.Rows[i]["OptionID"].ToString() != "" ) {
                                    dal.sqlStatment = "SELECT p1.MealID,STUFF(( SELECT ', '+OptionText FROM  MealOptions p2 WHERE p2.MealID = p1.MealID AND p2.OptionID IN (" + OsmanOptionMeals.Rows[i]["OptionID"].ToString() + ") ORDER BY OptionText FOR XML PATH('') ),1,1, '') AS Options FROM MealOptions p1 WHERE MealID = '" + OsmanOptionMeals.Rows[i]["MealID"].ToString() + "' GROUP BY MealID ";
                                    DataTable optionDt = dal.Query();
                                    if( optionDt.Rows.Count == 1 ) {
                                        OsmanOptionMeals.Rows[i]["MealName"] = OsmanOptionMeals.Rows[i]["MealName"] + " (" + optionDt.Rows[0]["Options"].ToString().TrimStart(' ') + ")";
                                    }
                                }
                            }

                            SummaryMealDt_Osam.Merge(OsmanOptionMeals);
                            SummaryMealDt_Osam.DefaultView.Sort = "MealID DESC";
                            SummaryMealDt_Osam = SummaryMealDt_Osam.DefaultView.ToTable();

                            if( SummaryMealDt_Osam.Rows.Count > 0 ) {
                                // check if normal meal exist
                                //DataRow[] Row = SummaryMealDt_Osam.Select("MealID = " + NormalMealID + "");
                                //if (Row.Length == 0)
                                //{
                                //    DataRow Newrow = SummaryMealDt_Osam.NewRow();
                                //    Newrow["MealName"] = MealDT.Rows[0]["MealName"].ToString();
                                //    Newrow["Count"] = "2";
                                //    Newrow["MealID"] = NormalMealID;
                                //    SummaryMealDt_Osam.Rows.Add(Newrow);
                                //}

                                for( int i = 0 ; i < SummaryMealDt_Osam.Rows.Count ; i++ ) {
                                    OsmanMealBody += "<tr>";
                                    OsmanMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:0;\">";
                                    OsmanMealBody += SummaryMealDt_Osam.Rows[i]["MealName"];
                                    OsmanMealBody += "</td>";
                                    OsmanMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:22px; padding-bottom:9px; padding-right:0;text-align:left\">";
                                    OsmanMealBody += SummaryMealDt_Osam.Rows[i]["Count"];
                                    OsmanMealBody += "</td>";
                                    OsmanMealBody += "</tr>";
                                }
                            } else {
                                OsmanMealBody += "<tr>";
                                OsmanMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:0;\">";
                                OsmanMealBody += " - ";
                                OsmanMealBody += "</td>";
                                OsmanMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:22px; padding-bottom:9px; padding-right:0;text-align:left\">";
                                OsmanMealBody += " - ";
                                OsmanMealBody += "</td>";
                                OsmanMealBody += "</tr>";
                            }

                            // send mail
                            //string p = System.Reflection.Assembly.GetEntryAssembly().Location;
                            StreamReader reader = new StreamReader("D:\\OrderReport.html");//(p.Replace("AklnyService.exe","OrderReport.html"));
                            string readFile = reader.ReadToEnd();
                            string StrContent = "";
                            StrContent = readFile;

                            //Here replace 
                            Object sumObject = SummaryMealDt_Osam.Compute("Sum(Count)", "");
                            string OsmanCount = sumObject.ToString();

                            sumObject = SummaryMealDt_Farid.Compute("Sum(Count)", "");
                            string FaridCount = sumObject.ToString();

                            int total = int.Parse(FaridCount) + int.Parse(OsmanCount);
                            string TotalCount = "" + total + "";

                            StrContent = StrContent.Replace("[FaridMeals]", FaridMealBody);
                            StrContent = StrContent.Replace("[OsmanMeals]", OsmanMealBody);
                            StrContent = StrContent.Replace("[FaridCount]", FaridCount);
                            StrContent = StrContent.Replace("[OsmanCount]", OsmanCount);
                            StrContent = StrContent.Replace("[TotalCount]", TotalCount);

                            ECS_Framework.Mail mail = new ECS_Framework.Mail();
                            //mail.mail.To.Add(new MailAddress("aya.abo.el.kheir@gmail.com"));
                            //mail.mail.To.Add(new MailAddress("aya.mohamed@ecs-co.com"));
                            mail.mail.To.Add(new MailAddress("heba.ahmed@ecs-co.com"));
                            mail.mail.To.Add(new MailAddress("ahmed.mubarak@ecs-co.com"));

                            mail.mail.IsBodyHtml = true;
                            mail.mail.Body = StrContent;
                            mail.mail.Subject = "ECS-Order Report";

                            if( mail.Send() ) {

                            } else {
                                ECS_Framework.Mail Failmail = new ECS_Framework.Mail();
                                Failmail.mail.To.Add(new MailAddress("aya.mohamed@ecs-co.com"));
                                Failmail.mail.IsBodyHtml = true;
                                Failmail.mail.Body = "Order Report was not sent successfully.";
                                Failmail.mail.Subject = "ECS-Order Report Sent Failed";

                                if( Failmail.Send() ) {

                                }
                            }
                        } else {
                            Utilities utilities = new Utilities();
                            utilities.WriteToFile("C:\\Enterprise Consultancy Services\\ECS Order Report Service\\", "ReportLog" + DateTime.Now.Date.Year + DateTime.Now.Date.Month.ToString().PadLeft(2, '0') + DateTime.Now.Date.Day.ToString().PadLeft(2, '0') + ".txt", "Settings Table is empty");

                        }
                    }
                }
            } catch( Exception e ) {
                string msg = e.Message;
                Utilities utilities = new Utilities();
                utilities.WriteToFile("C:\\Enterprise Consultancy Services\\ECS Order Report Service\\", "ReportLog" + DateTime.Now.Date.Year + DateTime.Now.Date.Month.ToString().PadLeft(2, '0') + DateTime.Now.Date.Day.ToString().PadLeft(2, '0') + ".txt", msg);
                ECS_Framework.Mail mail = new ECS_Framework.Mail();
                //mail.mail.To.Add(new MailAddress("aya.abo.el.kheir@gmail.com"));
                mail.mail.To.Add(new MailAddress("aya.mohamed@ecs-co.com"));
                // mail.mail.To.Add(new MailAddress("heba.ahmed@ecs-co.com"));


                mail.mail.IsBodyHtml = true;
                mail.mail.Body = msg;
                mail.mail.Subject = "ECS-Order Report Exception";

                if( mail.Send() ) {

                }



            }
        }

        public void SendEmail_AR() {
            // create Mail Body
            string FaridMealBody = "";
            string OsmanMealBody = "";
            string currentDate = DateTime.Now.ToString("yyyy-MM-dd");


            // get meals
            Database database = new Database();

            // check if today is a holiday
            DataTable Holiday = database.isHoliday();

            try {
                if( Holiday.Rows.Count == 0 ) {
                    // check if today is a weekend
                    DataTable Weekend = database.isWeekend();
                    if( Weekend.Rows.Count == 0 ) {
                        DataTable settingsDT = database.getSettings();
                        if( settingsDT.Rows.Count == 1 ) {
                            // get meals
                            string strartDate = settingsDT.Rows[0]["StartDate"].ToString();
                            DataTable MealDT = database.getMeals(currentDate, strartDate);

                            MSSQLDataAccessLayer dal = new MSSQLDataAccessLayer();
                            // Summary Farid
                            string NormalMealID = "";

                            if( MealDT.Rows.Count > 0 ) {
                                NormalMealID = MealDT.Rows[0]["MealID"].ToString();
                            }


                            // Summary Farid

                            // Meals without options
                            dal.sqlStatment = "SELECT ord.MealID,ord.OptionID,COUNT(ord.MealID) AS Count ,me.MealDescriptionAR AS MealName FROM Orders ord INNER JOIN Meal me ON ord.MealID=me.MealID WHERE ord.MealDate ='" + currentDate + "'  AND ord.LocationID='1' AND (ord.OptionID IS NULL OR ord.OptionID ='' ) GROUP BY ord.MealID,ord.optionID,me.MealDescriptionAR ";
                            DataTable SummaryMealDt_Farid = dal.Query();

                            // get office meals 
                            dal.sqlStatment = "SELECT ofmeal.MealID,ofmeal.Count,me.MealDescriptionAR AS MealName FROM Office_Meals ofmeal INNER JOIN Meal me ON ofmeal.MealID=me.MealID WHERE ofmeal.Day = (Select  DateNAME (dw,'" + currentDate + "')) AND ofmeal.LocationID='1' ";
                            DataTable OfficeMealsDt_Farid = dal.Query();
                            if( OfficeMealsDt_Farid.Rows.Count > 0 ) {
                                for( int i = 0 ; i < OfficeMealsDt_Farid.Rows.Count ; i++ ) {

                                    // add count to Summary meal if mealid exist
                                    DataRow[] foundRows = SummaryMealDt_Farid.Select("MealID =" + OfficeMealsDt_Farid.Rows[i]["MealID"].ToString());
                                    if( foundRows.Length == 1 ) {
                                        foundRows[0]["Count"] = "" + (int.Parse(foundRows[0]["Count"].ToString()) + int.Parse(OfficeMealsDt_Farid.Rows[i]["Count"].ToString()));
                                    } else {
                                        // doesn't exist so add it
                                        DataRow Newrow = SummaryMealDt_Farid.NewRow();
                                        Newrow["MealName"] = OfficeMealsDt_Farid.Rows[i]["MealName"].ToString();
                                        Newrow["Count"] = OfficeMealsDt_Farid.Rows[i]["Count"].ToString();
                                        Newrow["MealID"] = OfficeMealsDt_Farid.Rows[i]["MealID"].ToString();
                                        SummaryMealDt_Farid.Rows.Add(Newrow);
                                    }
                                }
                            }

                            // Meals with options
                            dal.sqlStatment = "SELECT ord.MealID,OptionID,Count(OptionID) AS Count ,me.MealDescriptionAR AS MealName FROM Orders ord INNER JOIN Meal me ON ord.MealID=me.MealID WHERE ord.OptionID !='' AND  MealDate ='" + currentDate + "'  AND ord.LocationID='1' group by ord.MealID,me.MealDescriptionAR, OptionID having count(*) >= 1";
                            DataTable FaridOptionMeals = dal.Query();
                            for( int i = 0 ; i < FaridOptionMeals.Rows.Count ; i++ ) {
                                if( FaridOptionMeals.Rows[i]["OptionID"].ToString() != "" ) {
                                    dal.sqlStatment = "SELECT p1.MealID,STUFF(( SELECT ', '+OptionTextAR FROM  MealOptions p2 WHERE p2.MealID = p1.MealID AND p2.OptionID IN (" + FaridOptionMeals.Rows[i]["OptionID"].ToString() + ") ORDER BY OptionTextAR FOR XML PATH('') ),1,1, '') AS Options FROM MealOptions p1 WHERE MealID = '" + FaridOptionMeals.Rows[i]["MealID"].ToString() + "' GROUP BY MealID ";
                                    DataTable optionDt = dal.Query();
                                    if( optionDt.Rows.Count == 1 ) {
                                        FaridOptionMeals.Rows[i]["MealName"] = FaridOptionMeals.Rows[i]["MealName"] + " " + optionDt.Rows[0]["Options"].ToString().TrimStart(' ') + " ";
                                    }
                                }
                            }

                            SummaryMealDt_Farid.Merge(FaridOptionMeals);
                            SummaryMealDt_Farid.DefaultView.Sort = "MealID DESC";
                            SummaryMealDt_Farid = SummaryMealDt_Farid.DefaultView.ToTable();
                            if( SummaryMealDt_Farid.Rows.Count > 0 ) {
                                // check if normal meal exist
                                //DataRow[] Row = SummaryMealDt_Farid.Select("MealID = " + NormalMealID + "");

                                //if (Row.Length == 0)
                                //{
                                //    DataRow Newrow = SummaryMealDt_Farid.NewRow();
                                //    Newrow["MealName"] = MealDT.Rows[0]["MealName"].ToString();
                                //    Newrow["Count"] = "2";
                                //    Newrow["MealID"] = NormalMealID;
                                //    SummaryMealDt_Farid.Rows.Add(Newrow);
                                //}
                                for( int i = 0 ; i < SummaryMealDt_Farid.Rows.Count ; i++ ) {
                                    FaridMealBody += "<tr>";
                                    FaridMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:22px; padding-bottom:9px; padding-right:0;text-align:left\">";
                                    FaridMealBody += SummaryMealDt_Farid.Rows[i]["Count"];
                                    FaridMealBody += "</td>";
                                    FaridMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:5px; padding-bottom:9px; padding-right:0;text-align:left\">";
                                    FaridMealBody += SummaryMealDt_Farid.Rows[i]["MealName"];
                                    FaridMealBody += "</td>";
                                    FaridMealBody += "</tr>";
                                }
                            } else {
                                FaridMealBody += "<tr>";
                                FaridMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:0;\">";
                                FaridMealBody += " - ";
                                FaridMealBody += "</td>";
                                FaridMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:22px; padding-bottom:9px; padding-right:0;text-align:left\">";
                                FaridMealBody += " - ";
                                FaridMealBody += "</td>";
                                FaridMealBody += "</tr>";
                            }


                            // Summary Osman

                            // Meals without options
                            //dal.sqlStatment = "SELECT ord.MealID,ord.OptionID,CASE WHEN ord.MealID ='" + NormalMealID + "' THEN COUNT(ord.MealID) + 2 ELSE COUNT(ord.MealID) END  AS Count ,me.MealName FROM Orders ord INNER JOIN Meal me ON ord.MealID=me.MealID WHERE ord.MealDate ='" + currentDate + "'  AND ord.LocationID='2' AND (ord.OptionID IS NULL OR ord.OptionID ='' ) GROUP BY ord.MealID,ord.optionID,me.MealName ";
                            dal.sqlStatment = "SELECT ord.MealID,ord.OptionID,COUNT(ord.MealID) AS Count ,me.MealDescriptionAR AS MealName FROM Orders ord INNER JOIN Meal me ON ord.MealID=me.MealID WHERE ord.MealDate ='" + currentDate + "'  AND ord.LocationID='2' AND (ord.OptionID IS NULL OR ord.OptionID ='' ) GROUP BY ord.MealID,ord.optionID,me.MealDescriptionAR ";
                            DataTable SummaryMealDt_Osam = dal.Query();


                            // get office meals 
                            dal.sqlStatment = "SELECT ofmeal.MealID,ofmeal.Count,me.MealDescriptionAR AS MealName FROM Office_Meals ofmeal INNER JOIN Meal me ON ofmeal.MealID=me.MealID WHERE ofmeal.Day = (Select  DateNAME (dw,'" + currentDate + "')) AND ofmeal.LocationID='2' ";
                            DataTable OfficeMealsDt_Osman = dal.Query();
                            if( OfficeMealsDt_Osman.Rows.Count > 0 ) {
                                for( int i = 0 ; i < OfficeMealsDt_Osman.Rows.Count ; i++ ) {

                                    // add count to Summary meal if mealid exist
                                    DataRow[] foundRows = SummaryMealDt_Osam.Select("MealID =" + OfficeMealsDt_Osman.Rows[i]["MealID"].ToString());
                                    if( foundRows.Length == 1 ) {
                                        foundRows[0]["Count"] = "" + (int.Parse(foundRows[0]["Count"].ToString()) + int.Parse(OfficeMealsDt_Osman.Rows[i]["Count"].ToString()));
                                    } else {
                                        // doesn't exist so add it
                                        DataRow Newrow = SummaryMealDt_Osam.NewRow();
                                        Newrow["MealName"] = OfficeMealsDt_Osman.Rows[i]["MealName"].ToString();
                                        Newrow["Count"] = OfficeMealsDt_Osman.Rows[i]["Count"].ToString();
                                        Newrow["MealID"] = OfficeMealsDt_Osman.Rows[i]["MealID"].ToString();
                                        SummaryMealDt_Osam.Rows.Add(Newrow);
                                    }
                                }
                            }

                            // Meals with options
                            dal.sqlStatment = "SELECT ord.MealID,OptionID,Count(OptionID) AS Count ,me.MealDescriptionAR AS MealName FROM Orders ord INNER JOIN Meal me ON ord.MealID=me.MealID WHERE ord.OptionID !='' AND  MealDate ='" + currentDate + "'  AND ord.LocationID='2' group by ord.MealID,me.MealDescriptionAR, OptionID having count(*) >= 1";
                            DataTable OsmanOptionMeals = dal.Query();
                            for( int i = 0 ; i < OsmanOptionMeals.Rows.Count ; i++ ) {
                                if( OsmanOptionMeals.Rows[i]["OptionID"].ToString() != "" ) {
                                    dal.sqlStatment = "SELECT p1.MealID,STUFF(( SELECT ', '+OptionTextAR FROM  MealOptions p2 WHERE p2.MealID = p1.MealID AND p2.OptionID IN (" + OsmanOptionMeals.Rows[i]["OptionID"].ToString() + ") ORDER BY OptionTextAR FOR XML PATH('') ),1,1, '') AS Options FROM MealOptions p1 WHERE MealID = '" + OsmanOptionMeals.Rows[i]["MealID"].ToString() + "' GROUP BY MealID ";
                                    DataTable optionDt = dal.Query();
                                    if( optionDt.Rows.Count == 1 ) {
                                        OsmanOptionMeals.Rows[i]["MealName"] = OsmanOptionMeals.Rows[i]["MealName"] + " " + optionDt.Rows[0]["Options"].ToString().TrimStart(' ') + " ";
                                    }
                                }
                            }

                            SummaryMealDt_Osam.Merge(OsmanOptionMeals);
                            SummaryMealDt_Osam.DefaultView.Sort = "MealID DESC";
                            SummaryMealDt_Osam = SummaryMealDt_Osam.DefaultView.ToTable();

                            if( SummaryMealDt_Osam.Rows.Count > 0 ) {
                                // check if normal meal exist
                                //DataRow[] Row = SummaryMealDt_Osam.Select("MealID = " + NormalMealID + "");
                                //if (Row.Length == 0)
                                //{
                                //    DataRow Newrow = SummaryMealDt_Osam.NewRow();
                                //    Newrow["MealName"] = MealDT.Rows[0]["MealName"].ToString();
                                //    Newrow["Count"] = "2";
                                //    Newrow["MealID"] = NormalMealID;
                                //    SummaryMealDt_Osam.Rows.Add(Newrow);
                                //}

                                for( int i = 0 ; i < SummaryMealDt_Osam.Rows.Count ; i++ ) {
                                    OsmanMealBody += "<tr>";
                                    OsmanMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:22px; padding-bottom:9px; padding-right:0;text-align:left\">";
                                    OsmanMealBody += SummaryMealDt_Osam.Rows[i]["Count"];
                                    OsmanMealBody += "</td>";
                                    OsmanMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:5px; padding-bottom:9px; padding-right:0;text-align:left\">";
                                    OsmanMealBody += SummaryMealDt_Osam.Rows[i]["MealName"];
                                    OsmanMealBody += "</td>";
                                    OsmanMealBody += "</tr>";
                                }
                            } else {
                                OsmanMealBody += "<tr>";
                                OsmanMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:0;\">";
                                OsmanMealBody += " - ";
                                OsmanMealBody += "</td>";
                                OsmanMealBody += "<td valign=\"top\" width=\"50%\" class=\"mcnTextContent\" style=\"padding-top:9px; padding-left:22px; padding-bottom:9px; padding-right:0;text-align:left\">";
                                OsmanMealBody += " - ";
                                OsmanMealBody += "</td>";
                                OsmanMealBody += "</tr>";
                            }

                            // send mail
                            //string p = System.Reflection.Assembly.GetEntryAssembly().Location;
                            StreamReader reader = new StreamReader("D:\\OrderReport.html");//(p.Replace("AklnyService.exe","OrderReport.html"));
                            string readFile = reader.ReadToEnd();
                            string StrContent = "";
                            StrContent = readFile;

                            //Here replace 
                            Object sumObject = SummaryMealDt_Osam.Compute("Sum(Count)", "");
                            string OsmanCount = sumObject.ToString();

                            sumObject = SummaryMealDt_Farid.Compute("Sum(Count)", "");
                            string FaridCount = sumObject.ToString();

                            int total = int.Parse(FaridCount) + int.Parse(OsmanCount);
                            string TotalCount = "" + total + "";

                            StrContent = StrContent.Replace("[FaridMeals]", FaridMealBody);
                            StrContent = StrContent.Replace("[OsmanMeals]", OsmanMealBody);
                            StrContent = StrContent.Replace("[FaridCount]", FaridCount);
                            StrContent = StrContent.Replace("[OsmanCount]", OsmanCount);
                            StrContent = StrContent.Replace("[TotalCount]", TotalCount);

                            ECS_Framework.Mail mail = new ECS_Framework.Mail();
                            //mail.mail.To.Add(new MailAddress("aya.abo.el.kheir@gmail.com"));

                            mail.mail.To.Add(new MailAddress("aboamar.st.fatima@gmail.com"));
                            mail.mail.To.Add(new MailAddress("heba.ahmed@ecs-co.com"));
                            mail.mail.To.Add(new MailAddress("ahmed.mubarak@ecs-co.com"));
                            mail.mail.To.Add(new MailAddress("marwa.hafez@ecs-co.com"));
                            mail.mail.To.Add(new MailAddress("aya.mohamed@ecs-co.com"));


                            mail.mail.IsBodyHtml = true;
                            mail.mail.Body = StrContent;
                            mail.mail.Subject = "ECS-Order Report";

                            if( mail.Send() ) {

                            } else {
                                ECS_Framework.Mail Failmail = new ECS_Framework.Mail();
                                Failmail.mail.To.Add(new MailAddress("aya.mohamed@ecs-co.com"));
                                Failmail.mail.IsBodyHtml = true;
                                Failmail.mail.Body = "Order Report was not sent successfully.";
                                Failmail.mail.Subject = "ECS-Order Report Sent Failed";

                                if( Failmail.Send() ) {

                                }
                            }
                        } else {
                            Utilities utilities = new Utilities();
                            utilities.WriteToFile("C:\\Enterprise Consultancy Services\\ECS Order Report Service\\", "ReportLog" + DateTime.Now.Date.Year + DateTime.Now.Date.Month.ToString().PadLeft(2, '0') + DateTime.Now.Date.Day.ToString().PadLeft(2, '0') + ".txt", "Settings Table is empty");

                        }
                    }
                }
            } catch( Exception e ) {
                string msg = e.Message;
                Utilities utilities = new Utilities();
                utilities.WriteToFile("C:\\Enterprise Consultancy Services\\ECS Order Report Service\\", "ReportLog" + DateTime.Now.Date.Year + DateTime.Now.Date.Month.ToString().PadLeft(2, '0') + DateTime.Now.Date.Day.ToString().PadLeft(2, '0') + ".txt", msg);
                ECS_Framework.Mail mail = new ECS_Framework.Mail();
                //mail.mail.To.Add(new MailAddress("aya.abo.el.kheir@gmail.com"));
                mail.mail.To.Add(new MailAddress("aya.mohamed@ecs-co.com"));
                // mail.mail.To.Add(new MailAddress("heba.ahmed@ecs-co.com"));


                mail.mail.IsBodyHtml = true;
                mail.mail.Body = msg;
                mail.mail.Subject = "ECS-Order Report Exception";

                if( mail.Send() ) {

                }



            }
        }

    }
}
