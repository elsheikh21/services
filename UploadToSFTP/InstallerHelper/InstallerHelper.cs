﻿using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Configuration;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Configuration.Install;

namespace UploadToSFTP {
    [RunInstaller(true)]
    public partial class InstallerHelper:Installer {
        
        public InstallerHelper() {
            InitializeComponent();
        }

        public override void Install(IDictionary stateSaver) {
            base.Install(stateSaver);

            string path = Context.Parameters["EDITA1"];
            string debugPath = path + "debug.txt";
            string sessionPath = path + "session.txt";
            string eventsPath = path + "log.txt";

            ApplySettings("DebugLogPath", debugPath);
            ApplySettings("SessionLogPath", sessionPath);
            ApplySettings("ResultsPath", eventsPath);

            bool cond1 = String.IsNullOrEmpty(debugPath) && String.IsNullOrWhiteSpace(debugPath);
            bool cond2 = String.IsNullOrEmpty(sessionPath) && String.IsNullOrWhiteSpace(sessionPath);
            bool cond3 = String.IsNullOrEmpty(eventsPath) && String.IsNullOrWhiteSpace(eventsPath);
            bool cond4 = cond1 && cond2 && cond3;

            //StreamWriter sw = new StreamWriter(@"C:\Users\ahmede\Desktop\txt2.txt", true);
            //sw.Write(debugPath + " SSSSS " + sessionPath + " SSSSS " + eventsPath + Environment.NewLine);
            //sw.Close();

            if( !cond4 ) {

            } else {

            }
        }

        protected override void OnAfterInstall(IDictionary savedState) {
            base.OnAfterInstall(savedState);
        }

        public void ApplySettings(string key, string value) {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            if( settings[key] == null ) {
                settings.Add(key, value);
            } else {
                settings[key].Value = value;
            }
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
        }
    }
}
